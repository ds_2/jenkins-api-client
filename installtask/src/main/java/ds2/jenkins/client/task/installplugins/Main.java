package ds2.jenkins.client.task.installplugins;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.JenkinsConfiguration;
import ds2.jenkins.client.api.JenkinsService;
import ds2.jenkins.client.api.JkPackage;
import ds2.jenkins.client.api.dto.*;
import ds2.jenkins.client.api.dto.updatecenter.Job;
import ds2.jenkins.client.api.dto.updatecenter.JobStatus;
import ds2.jenkins.client.api.dto.updatecenter.JobType;
import ds2.jenkins.client.impl.JenkinsServiceImpl;
import ds2.jenkins.client.impl.PluginManagerServiceImpl;
import ds2.jenkins.client.impl.UpdateCenterServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import static ds2.jenkins.client.task.installplugins.WorkMode.PERFORM_UPDATE;
import static ds2.jenkins.client.task.installplugins.WorkMode.PLAN_UPDATE;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        WorkMode mode = PLAN_UPDATE;
        boolean dryRun = false;
        if (args == null || args.length < 4) {
            LOG.error("Parameter count is less than expected!");
            System.exit(1);
        }
        LOG.info("Configure client..");
        try {
            JenkinsConfiguration configuration = JenkinsConfigurationBuilder.builder()
                    .jenkinsBaseUrl(new URL(args[0]))
                    .username(args[1])
                    .apiToken(args[2].toCharArray())
                    .build();
            JenkinsClientSession session = new JenkinsClientSession();
            Path dataFile = Paths.get(args[3]);
            if (!Files.isReadable(dataFile)) {
                throw new JenkinsClientException("Cannot read " + dataFile);
            }
            JenkinsService jenkinsService = new JenkinsServiceImpl();
            PluginManagerServiceImpl pluginManagerService = new PluginManagerServiceImpl();
            UpdateCenterServiceImpl updateCenterService = new UpdateCenterServiceImpl();
            updateCenterService.setJenkinsService(jenkinsService);
            pluginManagerService.setJenkinsService(jenkinsService);
            if (PERFORM_UPDATE.equals(mode)) {
                Properties props = new Properties();
                props.load(Files.newBufferedReader(dataFile, StandardCharsets.UTF_8));
                LOG.info("Plugins to install are: {}", props);
                LOG.info("Client is configured as {}", configuration);
                List<PluginData> installedPLugins = pluginManagerService.getInstalledPlugins(configuration, session);
                Set<String> pluginIds = installedPLugins.stream().map(PluginData::getShortName).collect(Collectors.toSet());
                LOG.info("Found the following plugins already installed: {}", pluginIds);
                if (props.size() > 0) {
                    List<JkPackage> plugins = new ArrayList<>(props.size());
                    for (String propName : props.stringPropertyNames()) {
                        JkPackageDto dto = new JkPackageDto(propName, props.getProperty(propName));
                        plugins.add(dto);
                    }
                    LOG.info("Sending plugins to install: {}", plugins);
                    Set<PluginValidation> configurationTest = pluginManagerService.prevalidateConfig(configuration, session, plugins);
                    LOG.info("Validation result: {}", configurationTest);
                    LOG.info("OK, let us install them now as {}", jenkinsService.whoAmI(configuration, session));
                    if (!dryRun) {
                        pluginManagerService.installNecessaryPlugins(configuration, session, plugins);
                        boolean requiresRestart = false;
                        while (true) {
                            Thread.sleep(1000);
                            List<Job> currentJobs = updateCenterService.getJobs(configuration, session);
                            boolean allJobsAreDone = true;
                            for (Job job : currentJobs) {
                                if (!JobType.InstallationJob.equals(job.getType())) {
                                    continue;
                                }
                                JobStatus jobStatus = job.getStatus();
                                if (!jobStatus.isSuccess()) {
                                    allJobsAreDone = false;
                                    break;
                                }
                                switch (jobStatus.getType()) {
                                    case SuccessButRequiresRestart:
                                        requiresRestart = true;
                                        break;
                                    default:
                                        LOG.info("Unmapped state: {}", job);
                                        break;
                                }
                            }
                            if (allJobsAreDone) {
                                break;
                            }

                        }
                        if (requiresRestart) {
                            jenkinsService.scheduleSafeRestart(configuration, session);
                        }
                    }
                }
            } else if (WorkMode.PLAN_UPDATE.equals(mode)) {
                List<UpdateRecommendation> missingPlugins = updateCenterService.getUpdates(configuration, session);
                LOG.info("Plugins missing: {}", missingPlugins);
            }
        } catch (MalformedURLException e) {
            LOG.error("Could not create target url!", e);
        } catch (JenkinsClientException e) {
            LOG.error("Error when dealing with the jenkins instance!", e);
        } catch (IOException e) {
            LOG.error("Error when reading the plugin data from disk!", e);
        } catch (InterruptedException e) {
            LOG.error("Error when waiting a little while..", e);
        }
    }
}
