package ds2.jenkins.client.task.installplugins;

public enum WorkMode {
    PLAN_UPDATE,
    PERFORM_UPDATE;
}
