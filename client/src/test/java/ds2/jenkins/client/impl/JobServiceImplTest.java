package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.JobsService;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JenkinsConfigurationDto;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class JobServiceImplTest {
    private JobsService jobsService;
    private JenkinsConfigurationDto configuration = new JenkinsConfigurationDto();
    private JenkinsClientSession session = new JenkinsClientSession();

    @BeforeClass
    public void onLoad() throws MalformedURLException {
        JobServiceImpl pluginManagerService1 = new JobServiceImpl();
        pluginManagerService1.setDtoMapper(new DtoMapper());
        jobsService = pluginManagerService1;
        configuration.setJenkinsBaseUrl(new URL("http://localhost:12345"));
        configuration.setApiToken("c1e026257860543f4109ae4c4d5bdf50".toCharArray());
        configuration.setUsername("ciadm");
    }

    @Test(enabled = false)
    public void getJobNames() throws JenkinsClientException {
        List<String> allJobNames = jobsService.getAllJobNames(configuration, session, null, null);
        assertNotNull(allJobNames);
        assertTrue(allJobNames.size() > 0);
    }
}
