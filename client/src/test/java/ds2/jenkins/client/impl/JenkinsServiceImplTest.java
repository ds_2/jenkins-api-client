package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.JenkinsService;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JenkinsConfigurationDto;
import ds2.jenkins.client.api.dto.JenkinsInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;

import static org.testng.Assert.assertNotNull;

public class JenkinsServiceImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private JenkinsService jenkinsService;
    private JenkinsConfigurationDto configuration = new JenkinsConfigurationDto();
    private JenkinsClientSession session = new JenkinsClientSession();

    @BeforeClass
    public void onLoad() throws MalformedURLException {
        jenkinsService = new JenkinsServiceImpl();
        configuration.setJenkinsBaseUrl(new URL("http://localhost:12345"));
        configuration.setApiToken("c1e026257860543f4109ae4c4d5bdf50".toCharArray());
        configuration.setUsername("ciadm");
    }

    @Test(enabled = false)
    public void testScheduleRestart1() throws JenkinsClientException {
        jenkinsService.scheduleSafeRestart(configuration, session);
    }

    @Test(enabled = false)
    public void testQuietDown() throws JenkinsClientException {
        jenkinsService.setQuietMode(configuration, session, false);
    }

    @Test(enabled = false)
    public void testInfo() throws JenkinsClientException {
        JenkinsInfo info = jenkinsService.getInfo(configuration, session);
        assertNotNull(info);
        LOG.info("Jenkins: {}", info);
    }
}