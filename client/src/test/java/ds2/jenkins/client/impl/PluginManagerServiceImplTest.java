package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.PluginManagerService;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JenkinsConfigurationDto;
import ds2.jenkins.client.api.dto.JkPackageDto;
import ds2.jenkins.client.api.dto.PluginData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class PluginManagerServiceImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private PluginManagerService pluginManagerService;
    private JenkinsConfigurationDto configuration = new JenkinsConfigurationDto();
    private JenkinsClientSession session = new JenkinsClientSession();

    @BeforeClass
    public void onLoad() throws MalformedURLException {
        PluginManagerServiceImpl pluginManagerService1 = new PluginManagerServiceImpl();
        pluginManagerService1.setJenkinsService(new JenkinsServiceImpl());
        pluginManagerService = pluginManagerService1;
        configuration.setJenkinsBaseUrl(new URL("http://localhost:12345"));
        configuration.setApiToken("1193631413162146ca9d1712d34e138f4a".toCharArray());
        configuration.setUsername("ciadm");
    }

    @Test(enabled = true)
    public void testGetAllPlugins() throws JenkinsClientException {
        List<PluginData> result = pluginManagerService.getInstalledPlugins(configuration, session);
        assertNotNull(result);

    }

    @Test(enabled = true)
    public void testGetAllUpdates() throws JenkinsClientException {
        List<PluginData> result = pluginManagerService.getOutdatedOrMissingPlugins(configuration, session);
        assertNotNull(result);
        LOG.info("To be updated: {}", result.stream().map(PluginData::getShortName).sorted().collect(Collectors.toList()));
        LOG.info("Count: {}", result.size());
    }

    @Test(enabled = false)
    public void testInstall() throws JenkinsClientException {
        pluginManagerService.installOrUpdatePlugin(configuration, session, new JkPackageDto("workflow-support"));
        assertTrue(true);
    }

    @Test(enabled = true)
    public void testPrevalidate() throws JenkinsClientException {
        List<PluginData> result = pluginManagerService.getOutdatedOrMissingPlugins(configuration, session);
        assertNotNull(result);
        List<String> updatePlugins = result.stream().map(PluginData::getShortName).sorted().collect(Collectors.toList());
        LOG.info("To be updated: {}", updatePlugins);
        for (PluginData p : result) {
            LOG.info("Plugin {}: {}", p, pluginManagerService.prevalidateConfig(configuration, session, Collections.singleton(new JkPackageDto(p.getShortName(), p.getVersion()))));
        }
        assertTrue(true);
    }
}
