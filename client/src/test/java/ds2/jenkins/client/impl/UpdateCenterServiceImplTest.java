package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.UpdateCenterService;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JenkinsConfigurationDto;
import ds2.jenkins.client.api.dto.UpdateRecommendation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.testng.Assert.assertNotNull;

public class UpdateCenterServiceImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private UpdateCenterService to;
    private JenkinsConfigurationDto configuration = new JenkinsConfigurationDto();
    private JenkinsClientSession session = new JenkinsClientSession();

    @BeforeClass
    public void onLoad() throws MalformedURLException {
        UpdateCenterServiceImpl pluginManagerService1 = new UpdateCenterServiceImpl();
        pluginManagerService1.setJenkinsService(new JenkinsServiceImpl());
        to = pluginManagerService1;
        configuration.setJenkinsBaseUrl(new URL("http://localhost:12345"));
        configuration.setApiToken("1193631413162146ca9d1712d34e138f4a".toCharArray());
        configuration.setUsername("ciadm");
    }

    @Test
    public void testGetSites() throws JenkinsClientException {
        List<String> result = to.getAllUpdateCenters(configuration, session);
        assertNotNull(result);
    }

    @Test
    public void testGetUpdates() throws JenkinsClientException {
        List<UpdateRecommendation> result = to.getUpdates(configuration, session);
        assertNotNull(result);
    }
}
