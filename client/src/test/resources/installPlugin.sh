#!/usr/bin/env bash

# see https://github.com/vfarcic/ms-lifecycle/issues/3

adminPass="admin"
crumbLine=$(curl --user ciadm:$adminPass localhost':12345/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')

responseCode=0

echo "Validating possible installation.."
curl -X POST --user ciadm:$adminPass -H "$crumbLine" -d @updatePlugin.xml -H 'Content-Type: text/xml' http://localhost:12345/pluginManager/prevalidateConfig

echo ""
echo "Will try to install plugin(s) now if they are not existing here.."
responseCode=$(curl -X POST --user ciadm:$adminPass -H "$crumbLine" --write-out %{http_code} -d @updatePlugin.xml -H 'Content-Type: text/xml' http://localhost:12345/pluginManager/installNecessaryPlugins)

if [ "$responseCode" -lt "400" ]
then
    echo "OK, because http $responseCode"
else
    echo "Sry, error because http $responseCode"
    exit 1
fi

#response=$(curl -X POST --user ciadm:$adminPass -H "$crumbLine" --write-out %{http_code} localhost:12345/safeRestart)