package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.JenkinsConfiguration;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.impl.filters.AuthenticationHeaderAppender;
import ds2.jenkins.client.impl.filters.CrumbHeaderAppender;
import ds2.jenkins.client.impl.filters.JenkinsSessionAppender;
import ds2.jenkins.client.impl.filters.RequestHeaderLogger;
import ds2.jersey.media.json.gson.GsonProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;

public class JaxRsBase {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private ClientBuilder clientBuilder;


    protected Client getClient(JenkinsConfiguration configuration, JenkinsClientSession session) {
        LOG.debug("Will create new client from clientbuilder now..");
        if (clientBuilder == null) {
            LOG.debug("Will create new clientbuilder..");
            clientBuilder = ClientBuilder.newBuilder();
        }
        Client client = clientBuilder.build();
        client.register(new AuthenticationHeaderAppender(configuration));
        client.register(new CrumbHeaderAppender(session));
        client.register(new JenkinsSessionAppender(session));
        client.register(new RequestHeaderLogger());
        client.register(GsonProvider.class);
        LOG.debug("Done, have fun with this client now");
        return client;
    }

    protected <E> E withResponse(Response response, ResponseHandler handler) throws JenkinsClientException {
        E rc = handler.handleResponse(response);
        closeResponse(response);
        return rc;
    }

    protected WebTarget getClientWithDefaultTarget(JenkinsConfiguration configuration, JenkinsClientSession session) throws URISyntaxException {
        return getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI());
    }

    protected void closeResponse(Response response) {
        if (response != null) {
            response.close();
        }
    }

}
