package ds2.jenkins.client.impl.filters;

import ds2.jenkins.client.api.JenkinsConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationHeaderAppender implements ClientRequestFilter {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private JenkinsConfiguration configuration;

    public AuthenticationHeaderAppender(JenkinsConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void filter(ClientRequestContext requestContext) {
        LOG.debug("Adding header for authorization..");
        MultivaluedMap<String, Object> currentHeaders = requestContext.getHeaders();
        String base64Data = configuration.getUsername() + ":" + new String(configuration.getApiToken());
        String base64Value = Base64.getEncoder().encodeToString(base64Data.getBytes(StandardCharsets.UTF_8));
        LOG.trace("Base64 value to use as auth is {}", base64Value);
        currentHeaders.putSingle(HttpHeaders.AUTHORIZATION, "Basic " + base64Value);
    }
}
