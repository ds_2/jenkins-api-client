package ds2.jenkins.client.impl.filters;

import ds2.jenkins.client.api.dto.CookieData;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class JenkinsSessionAppender implements ClientRequestFilter, ClientResponseFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsSessionAppender.class);
    private final JenkinsClientSession configuration;
    private static final String HEADERNAME = "X-Jenkins-Session";

    public JenkinsSessionAppender(JenkinsClientSession configuration) {
        this.configuration = configuration;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        if (configuration.getJenkinsSession() != null && configuration.getJenkinsSession().trim().length() > 0) {
            LOGGER.debug("Setting session id {}..", configuration.getJenkinsSession());
            requestContext.getHeaders().putSingle(HEADERNAME, configuration.getJenkinsSession());
            requestContext.getHeaders().put("Cookies", configuration.getCookies().stream().map(c -> {
                return (Object) (c.getName() + "=" + c.getValue() + ";");
            }).collect(Collectors.toList()));
            //requestContext.getCookies().putAll(toCookies(configuration.getCookies()));
        } else {
            LOGGER.info("Looks like I have no sessionId ..");
        }
    }

    private Map<? extends String, ? extends Cookie> toCookies(Set<CookieData> cookies) {
        Map<String, Cookie> rcMap = new HashMap<>();
        for (CookieData cookieData : cookies) {
            String name = cookieData.getName();
            String value = cookieData.getValue();
            Cookie cookie = new Cookie(name, value);
            rcMap.put(name, cookie);
        }
        return rcMap;
    }

    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
        if (responseContext.getHeaderString(HEADERNAME) != null) {
            for (String cookieName : responseContext.getCookies().keySet()) {
                if (cookieName.startsWith("JSESSIONID")) {
                    configuration.getCookies().add(toCookie(responseContext.getCookies().get(cookieName)));
                } else {
                    LOGGER.debug("Found a non-session cookie named {}", cookieName);
                }
            }
            configuration.setJenkinsSession(responseContext.getHeaders().getFirst(HEADERNAME));
            LOGGER.info("Received sessionId: {}", configuration.getJenkinsSession());
        }
    }

    private CookieData toCookie(NewCookie newCookie) {
        if (newCookie == null) {
            return null;
        }
        CookieData cookieData = new CookieData();
        cookieData.setDomain(newCookie.getDomain());
        cookieData.setValue(newCookie.getValue());
        cookieData.setPath(newCookie.getPath());
        cookieData.setName(newCookie.getName());
        cookieData.setExpiry(newCookie.getExpiry());
        cookieData.setMaxAge(newCookie.getMaxAge());
        cookieData.setComment(newCookie.getComment());
        return cookieData;
    }
}
