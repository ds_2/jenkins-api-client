package ds2.jenkins.client.impl.filters;

import ds2.jenkins.client.api.dto.JenkinsClientSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.invoke.MethodHandles;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class CrumbHeaderAppender implements ClientRequestFilter, ClientResponseFilter {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private JenkinsClientSession configuration;

    public CrumbHeaderAppender(JenkinsClientSession configuration) {
        this.configuration = configuration;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        String method = requestContext.getMethod();
        if ("post".equalsIgnoreCase(method) && configuration.getCrumbData() != null) {
            LOG.debug("Adding crumb {} to POST request..", configuration.getCrumbData());
            requestContext.getHeaders().putSingle(configuration.getCrumbData().getCrumbRequestField(), configuration.getCrumbData().getCrumb());
        }
    }

    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
        LOG.debug("Here is the crumb filter! Headers are: {}", responseContext.getHeaders());
        if (configuration.getCrumbData() == null) {
            LOG.debug("Nothing to do here.");
            return;
        }
        String foundCrumb = responseContext.getHeaderString(configuration.getCrumbData().getCrumbRequestField());
        if (foundCrumb != null && foundCrumb.trim().length() > 0) {
            LOG.debug("Got a new crumb in return? {} vs old {}", foundCrumb, configuration.getCrumbData().getCrumb());
        }
    }
}
