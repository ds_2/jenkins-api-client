package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;

import javax.ws.rs.core.Response;

@FunctionalInterface
public interface ResponseHandler {
    <E> E handleResponse(Response response) throws JenkinsClientException;
}
