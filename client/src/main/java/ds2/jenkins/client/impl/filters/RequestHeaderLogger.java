package ds2.jenkins.client.impl.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.invoke.MethodHandles;

@Provider
@Priority(Priorities.HEADER_DECORATOR)
public class RequestHeaderLogger implements ClientRequestFilter {
    private static final Logger LOG=LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        LOG.debug("Headers to be sent: {}", requestContext.getHeaders());
    }
}
