package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.*;
import ds2.jenkins.client.api.dto.*;
import ds2.jenkins.client.api.helpers.Methods;
import ds2.jenkins.client.impl.dto.PluginsList;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

@Setter
public class PluginManagerServiceImpl extends JaxRsBase implements PluginManagerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private JenkinsService jenkinsService;

    @Override
    public List<PluginData> getInstalledPlugins(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        LOGGER.debug("getting installed plugins via {}", configuration);
        if (session.getCrumbData() == null || session.getCrumbData().isEmpty()) {
            LOGGER.info("Need crumb data first..");
            session.setCrumbData(jenkinsService.getCrumb(configuration, session));
        }
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("pluginManager/api/json");
            webTarget = webTarget.queryParam("depth", 2);
            Response response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body: {}", body);
                response.close();
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            PluginsList resultList = response.readEntity(PluginsList.class);
            response.close();
            LOGGER.debug("Got this data: {}", resultList);
            return resultList.getPlugins();
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        }
    }

    @Override
    public List<PluginData> getOutdatedOrMissingPlugins(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        LOGGER.debug("getting installed plugins via {}", configuration);
        if (session.getCrumbData() == null || session.getCrumbData().isEmpty()) {
            LOGGER.info("Need crumb data first..");
            session.setCrumbData(jenkinsService.getCrumb(configuration, session));
        }
        List<PluginData> installedPlugins = getInstalledPlugins(configuration, session);
        if (installedPlugins != null) {
            return installedPlugins.stream().filter(PluginData::isHasUpdate).sorted(Comparator.comparing(PluginData::getShortName)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public Set<PluginValidation> prevalidateConfig(JenkinsConfiguration configuration, JenkinsClientSession session, Collection<JkPackage> jkPackage) throws JenkinsClientException {
        LOGGER.debug("will try to validate plugin {} via {}", jkPackage, configuration);
        if (session.getCrumbData() == null || session.getCrumbData().isEmpty()) {
            LOGGER.info("Need crumb data first..");
            session.setCrumbData(jenkinsService.getCrumb(configuration, session));
        }
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("pluginManager/prevalidateConfig");
            StringBuilder xmlString = new StringBuilder(200);
            xmlString.append("<jenkins>");
            for (JkPackage pkg : jkPackage) {
                String versionToUse = "latest";
                if (!Methods.isBlank(pkg.getVersion())) {
                    versionToUse = pkg.getVersion();
                }
                xmlString.append("<install plugin=\"").append(pkg.getShortName()).append("@").append(versionToUse).append("\" />\n");
            }
            xmlString.append("</jenkins>\n");
            LOGGER.debug("Sending this xml: {}", xmlString.toString());
            Entity xmlEntity = Entity.xml(xmlString.toString());
            Response response = webTarget.request().post(xmlEntity);
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Error body: {}", body);
                throw new JenkinsClientException("Not allowing a plugin update/installation due to HTTP " + statusCode);
            }
            GenericType<List<PluginValidation>> targetType = new GenericType<List<PluginValidation>>() {
            };
            List<PluginValidation> responseResult = response.readEntity(targetType);
            response.close();
            LOGGER.debug("Done with query. Jenkins should now install the plugin. Visit the updateCenter and perform a safe restart!");
            return new HashSet<>(responseResult);
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        }
    }

    @Override
    public void installNecessaryPlugins(JenkinsConfiguration configuration, JenkinsClientSession session, Collection<JkPackage> pluginsWithVersion) throws JenkinsClientException {
        LOGGER.debug("will try to validate plugin {} via {}", pluginsWithVersion, configuration);
        if (session.getCrumbData() == null || session.getCrumbData().isEmpty()) {
            LOGGER.info("Need crumb data first..");
            session.setCrumbData(jenkinsService.getCrumb(configuration, session));
        }
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("pluginManager/installNecessaryPlugins");
            StringBuilder xmlString = new StringBuilder(200);
            xmlString.append("<jenkins>\n");
            for (JkPackage jkPackage : pluginsWithVersion) {
                String versionToUse = "current";
                if (!Methods.isBlank(jkPackage.getVersion())) {
                    versionToUse = jkPackage.getVersion();
                }
                xmlString.append("<install plugin=\"").append(jkPackage.getShortName()).append("@").append(versionToUse).append("\" />\n");
            }
            xmlString.append("</jenkins>\n");
            LOGGER.debug("Sending this xml: {}", xmlString.toString());
            Entity xmlEntity = Entity.xml(xmlString.toString());
            Response response = webTarget.request()
                    .accept("*/*")
                    .header("Referer", "http://localhost:12345/updateCenter")
                    .post(xmlEntity);
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Error body: {}", body);
                throw new JenkinsClientException("Not allowing a plugin update/installation due to HTTP " + statusCode);
            }
            String responseResult = response.readEntity(String.class);
            response.close();
            LOGGER.debug("Result sent by API: {}", responseResult);
            LOGGER.debug("Done with query. Jenkins should now install the plugin. Visit the updateCenter and perform a safe restart!");
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        }
    }

    @Override
    public void uninstallPlugin(JenkinsConfiguration configuration, String shortName) throws JenkinsClientException {

    }

    @Override
    public void installOrUpdatePlugin(JenkinsConfigurationDto configuration, JenkinsClientSession session, JkPackageDto jkPackageDto) throws JenkinsClientException {
        installNecessaryPlugins(configuration, session, Collections.singleton(jkPackageDto));
    }
}
