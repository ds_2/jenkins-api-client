package ds2.jenkins.client.impl.dto;

import ds2.jenkins.client.api.dto.updatecenter.InstallablePackage;
import ds2.jenkins.client.api.dto.updatecenter.Job;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Setter
@Getter
@ToString
@XmlRootElement(name = "updateCenterSite")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCenterSite {
    private String url;
    private boolean hasUpdates;
    private String id;
    private String connectionCheckUrl;
    @XmlElementWrapper
    private List<InstallablePackage> updates;
    @XmlElementWrapper
    private List<InstallablePackage> availables;
}
