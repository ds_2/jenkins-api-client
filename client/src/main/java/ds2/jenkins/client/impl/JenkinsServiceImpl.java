package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.JenkinsConfiguration;
import ds2.jenkins.client.api.JenkinsService;
import ds2.jenkins.client.api.dto.CrumbData;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JenkinsInfo;
import ds2.jenkins.client.api.dto.whoami.WhoAmIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;

public class JenkinsServiceImpl extends JaxRsBase implements JenkinsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public JenkinsInfo getInfo(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        LOGGER.debug("getting info via {}", configuration);
        Response response = null;
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("api/json");
            response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type=", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            JenkinsInfo jenkinsInfo = response.readEntity(JenkinsInfo.class);
            LOGGER.debug("Got this info: {}", jenkinsInfo);
            return jenkinsInfo;
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        } finally {
            closeResponse(response);
        }
    }

    @Override
    public CrumbData getCrumb(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        LOGGER.debug("getting crumb via {}", configuration);
        Response response = null;
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("crumbIssuer/api/json");
            response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            CrumbData crumbData = response.readEntity(CrumbData.class);
            LOGGER.debug("Got this data: {}", crumbData);
            return crumbData;
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        } finally {
            closeResponse(response);
        }
    }

    @Override
    public void scheduleSafeRestart(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        if (session.getCrumbData() == null || session.getCrumbData().isEmpty()) {
            LOGGER.info("Need crumb data first..");
            session.setCrumbData(getCrumb(configuration, session));
        }
        LOGGER.debug("Will perform safe restart using {}", configuration);
        Response response = null;
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("safeRestart");
            response = webTarget.request().buildPost(null).invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body of error response: {}", body);
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        } finally {
            closeResponse(response);
        }
    }

    @Override
    public void setQuietMode(JenkinsConfiguration configuration, JenkinsClientSession session, boolean quiet) throws JenkinsClientException {
        if (session.getCrumbData() == null || session.getCrumbData().isEmpty()) {
            LOGGER.info("Need crumb data first..");
            session.setCrumbData(getCrumb(configuration, session));
        }
        LOGGER.debug("Will set or cancel the quiet mode now using config {}", configuration);
        Response response = null;
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path(quiet ? "quietDown" : "cancelQuietDown");
            response = webTarget.request().buildPost(null).invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body of error response: {}", body);
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        } finally {
            closeResponse(response);
        }
    }

    @Override
    public WhoAmIResponse whoAmI(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        if (session.getCrumbData() == null || session.getCrumbData().isEmpty()) {
            LOGGER.info("Need crumb data first..");
            session.setCrumbData(getCrumb(configuration, session));
        }
        LOGGER.debug("Will set or cancel the quiet mode now using config {}", configuration);
        Response response = null;
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("/whoAmI/api/xml");
            response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body of error response: {}", body);
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            WhoAmIResponse response1 = response.readEntity(WhoAmIResponse.class);
            return response1;
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        } finally {
            closeResponse(response);
        }
    }
}
