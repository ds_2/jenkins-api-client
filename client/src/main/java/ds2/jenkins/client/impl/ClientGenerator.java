package ds2.jenkins.client.impl;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class ClientGenerator {
    public Client generateClient() {
        Client client = ClientBuilder.newClient();
        return client;
    }
}
