package ds2.jenkins.client.impl.dto;

import ds2.jenkins.client.api.dto.updatecenter.Job;
import lombok.*;

import javax.xml.bind.annotation.*;
import java.util.List;

@Setter
@Getter
@ToString
@XmlRootElement(name = "localUpdateCenter")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCenterResult {
    @XmlElement
    @XmlElementWrapper
    private List<UpdateCenterSite> sites;
    @XmlElement
    @XmlElementWrapper
    private List<Job> jobs;
}
