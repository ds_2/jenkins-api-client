package ds2.jenkins.client.impl.dto;

import ds2.jenkins.client.api.dto.PluginData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
@XmlRootElement(name = "localPluginManager")
@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor
public class PluginsList {
    @XmlElement(name = "plugin")
    private List<PluginData> plugins;

    public PluginsList() {
        plugins = new ArrayList<>(1);
    }
}
