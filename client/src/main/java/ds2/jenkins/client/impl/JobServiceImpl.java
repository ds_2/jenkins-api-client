package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.JenkinsConfiguration;
import ds2.jenkins.client.api.JobsService;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JobResult;
import ds2.jenkins.client.api.dto.JobState;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Setter
public class JobServiceImpl extends JaxRsBase implements JobsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private DtoMapper dtoMapper;

    @Override
    public String downloadJobConfiguration(JenkinsConfiguration configuration, JenkinsClientSession session, String jobName) throws JenkinsClientException {
        return null;
    }

    @Override
    public List<String> getAllJobNames(JenkinsConfiguration configuration, JenkinsClientSession session, Set<JobState> states, Set<JobResult> results) throws JenkinsClientException {
        try {
            WebTarget webTarget = getClientWithDefaultTarget(configuration, session).path("api/json");
            webTarget = webTarget.queryParam("depth", 3);
            Response response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body: {}", body);
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            return null;
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        }
    }

    @Override
    public void scheduleBuild(JenkinsConfiguration configuration, JenkinsClientSession session, String jobName, Map<String, Object> buildParams) throws JenkinsClientException {

    }

    @Override
    public void enableJob(JenkinsConfiguration configuration, JenkinsClientSession session, String name, boolean enable) throws JenkinsClientException {

    }
}
