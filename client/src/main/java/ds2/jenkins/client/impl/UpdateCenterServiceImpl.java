package ds2.jenkins.client.impl;

import ds2.jenkins.client.api.JenkinsClientException;
import ds2.jenkins.client.api.JenkinsConfiguration;
import ds2.jenkins.client.api.JenkinsService;
import ds2.jenkins.client.api.UpdateCenterService;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.Tree;
import ds2.jenkins.client.api.dto.UpdateRecommendation;
import ds2.jenkins.client.api.dto.updatecenter.InstallablePackage;
import ds2.jenkins.client.api.dto.updatecenter.Job;
import ds2.jenkins.client.impl.dto.UpdateCenterResult;
import ds2.jenkins.client.impl.dto.UpdateCenterSite;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UpdateCenterServiceImpl extends JaxRsBase implements UpdateCenterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Setter
    private JenkinsService jenkinsService;

    @Override
    public List<String> getAllUpdateCenters(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        LOGGER.debug("getting installed plugins via {}", configuration);
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("updateCenter/api/json");
            webTarget = webTarget.queryParam("depth", 1);
            Response response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body: {}", body);
                response.close();
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            UpdateCenterResult resultList = response.readEntity(UpdateCenterResult.class);
            response.close();
            List<String> siteUrls = resultList.getSites().stream().map(UpdateCenterSite::getUrl).filter(Objects::nonNull).collect(Collectors.toList());
            LOGGER.debug("Got this data: {}", resultList);
            return siteUrls;
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        }
    }

    @Override
    public List<UpdateRecommendation> getUpdates(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        LOGGER.debug("getting installed plugins via {}", configuration);
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("updateCenter/api/json");
            webTarget = webTarget.queryParam("depth", 3);
            Response response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body: {}", body);
                response.close();
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            UpdateCenterResult resultList = response.readEntity(UpdateCenterResult.class);
            response.close();
            LOGGER.trace("Got this data: {}", resultList);
            List<UpdateRecommendation> updates = new ArrayList<>(100);
            final Tree<String> rootTree = new Tree<>();
            for (UpdateCenterSite s : resultList.getSites().stream().filter(UpdateCenterSite::isHasUpdates).collect(Collectors.toList())) {
                for (InstallablePackage s2 : s.getUpdates()) {
                    UpdateRecommendation jkPackageDto = new UpdateRecommendation();
                    jkPackageDto.setId(s2.getName());
                    Tree<String> leaf = rootTree.findInTree(s2.getName());
                    if (leaf == null) {
                        leaf = rootTree.addLeaf(s2.getName());
                    }
                    jkPackageDto.setNewVersion(s2.getVersion());
                    jkPackageDto.setInstalledVersion(s2.getInstalled().getVersion());
                    jkPackageDto.setCompatibleWithInstalledVersion(s2.isCompatibleWithInstalledVersion());
                    jkPackageDto.setName(s2.getTitle());
                    if (s2.getNeededDependencies() != null && !s2.getNeededDependencies().isEmpty()) {
                        jkPackageDto.setDependencies(new HashMap<>(10));
                        for (InstallablePackage s3 : s2.getNeededDependencies()) {
                            Tree<String> existingLeaf = rootTree.findInTree(s3.getName());
                            if (existingLeaf == null) {
                                leaf.addLeaf(s3.getName());
                            } else {
                                leaf.moveLeafToHere(existingLeaf);
                            }
                            jkPackageDto.getDependencies().put(s3.getName(), s3.getVersion());
                        }
                    }
                    updates.add(jkPackageDto);
                }
            }
            LOGGER.debug("Updates are: {}", updates);
            LOGGER.debug("Tree: {}", rootTree.printLeaf(0));
            return updates;
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        }
    }

    @Override
    public List<Job> getJobs(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException {
        LOGGER.debug("getting installed plugins via {}", configuration);
        try {
            WebTarget webTarget = getClient(configuration, session).target(configuration.getJenkinsBaseUrl().toURI()).path("updateCenter/api/json");
            webTarget = webTarget.queryParam("depth", 2);
            Response response = webTarget.request().buildGet().invoke();
            int statusCode = response.getStatus();
            LOGGER.debug("Response is http {}, type={}", statusCode, response.getMediaType());
            if (statusCode >= 400) {
                String body = response.readEntity(String.class);
                LOGGER.debug("Body: {}", body);
                response.close();
                throw new JenkinsClientException("Server responded with http code " + statusCode);
            }
            UpdateCenterResult resultList = response.readEntity(UpdateCenterResult.class);
            response.close();
            LOGGER.debug("Got this data: {}", resultList.getJobs());
            return resultList.getJobs();
        } catch (URISyntaxException e) {
            throw new JenkinsClientException("URI error!", e);
        }
    }

}
