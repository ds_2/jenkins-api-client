package ds2.jenkins.client.api.helpers;

public interface Methods {
    static boolean isBlank(String s) {
        if (s == null || s.trim().length() <= 0) {
            return true;
        }
        return false;
    }
}
