package ds2.jenkins.client.api.dto;

import ds2.jenkins.client.api.helpers.Methods;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "defaultCrumbIssuer")
public class CrumbData {
    @XmlElement
    private String crumb;
    @XmlElement
    private String crumbRequestField;

    public boolean isEmpty() {
        return Methods.isBlank(crumb) || Methods.isBlank(crumbRequestField);
    }
}
