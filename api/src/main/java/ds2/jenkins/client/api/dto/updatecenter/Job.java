package ds2.jenkins.client.api.dto.updatecenter;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@ToString
@XmlRootElement(name = "updateCenterJob")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class Job {
    private String errorMessage;
    private int id;
    private JobType type;
    private String name;
    private JobStatus status;
    private InstallablePackage plugin;
}
