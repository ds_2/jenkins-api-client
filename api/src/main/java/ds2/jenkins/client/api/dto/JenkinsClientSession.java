package ds2.jenkins.client.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@ToString
public class JenkinsClientSession {
    private Set<CookieData> cookies = new HashSet<>(1);
    private CrumbData crumbData;
    private String jenkinsSession;
}
