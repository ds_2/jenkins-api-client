package ds2.jenkins.client.api.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class UpdateRecommendation implements Serializable {
    private String id;
    private String name;
    private String newVersion;
    private String installedVersion;
    private boolean compatibleWithInstalledVersion;
    private Map<String, String> dependencies;
}
