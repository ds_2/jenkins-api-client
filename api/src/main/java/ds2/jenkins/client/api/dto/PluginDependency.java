package ds2.jenkins.client.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"shortName"})
@XmlRootElement(name = "pluginDataDependency")
public class PluginDependency {
    private boolean optional;
    private String shortName;
    private String version;
}
