package ds2.jenkins.client.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

/**
 * Similar to JenkinsModelJenkins.
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class JenkinsInfo {
    private Set<Label> assignedLabels;
    private String mode;
    private String nodeDescription;
    private String nodeName;
    private int numExecutors;
    private String description;
    private boolean quietingDown;
    private int slaveAgentPort;
    private boolean useCrumbs;
    private boolean useSecurity;
    private Set<JobData> jobs;
    private List<JenkinsView> views;
    private JenkinsView primaryView;
}
