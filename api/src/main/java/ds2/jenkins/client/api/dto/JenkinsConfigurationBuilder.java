package ds2.jenkins.client.api.dto;

import ds2.jenkins.client.api.JenkinsConfiguration;
import lombok.Builder;
import lombok.Data;

import java.net.URL;

@Data
@Builder
public class JenkinsConfigurationBuilder implements JenkinsConfiguration {
    private String username;
    private char[] apiToken;
    private URL jenkinsBaseUrl;
    @Builder.Default
    private int readTimeoutSeconds = 15;
    @Builder.Default
    private int connectTimeoutSeconds = 10;
    private CrumbData crumbData;
    private String jenkinsSession;
}
