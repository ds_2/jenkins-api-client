package ds2.jenkins.client.api;

import ds2.jenkins.client.api.dto.CrumbData;

import java.net.URL;

public interface JenkinsConfiguration {
    String getUsername();

    URL getJenkinsBaseUrl();

    int getConnectTimeoutSeconds();

    int getReadTimeoutSeconds();

    char[] getApiToken();

}
