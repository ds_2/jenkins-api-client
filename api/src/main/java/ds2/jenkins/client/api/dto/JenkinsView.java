package ds2.jenkins.client.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class JenkinsView {
    private String _class;
    private String name;
    private String description;
    private String url;
}
