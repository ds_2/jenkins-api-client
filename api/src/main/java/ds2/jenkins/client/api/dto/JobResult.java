package ds2.jenkins.client.api.dto;

public enum JobResult {
    SUCCESSFUL, FAILED, TIMEDOUT, CANCELLED;
}
