package ds2.jenkins.client.api;

import ds2.jenkins.client.api.dto.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface PluginManagerService {
    /**
     * See URL /pluginManager/api/json?depth=3
     *
     * @param configuration the jaxrs config
     * @return the list of installed plugins
     */
    List<PluginData> getInstalledPlugins(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;

    /**
     * Returns all outdated or missing plugins.
     *
     * @param configuration the jaxrs config
     * @return
     * @throws JenkinsClientException
     */
    List<PluginData> getOutdatedOrMissingPlugins(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;

    Set<PluginValidation> prevalidateConfig(JenkinsConfiguration configuration, JenkinsClientSession session, Collection<JkPackage> jkPackage) throws JenkinsClientException;

    void installNecessaryPlugins(JenkinsConfiguration configuration, JenkinsClientSession session, Collection<JkPackage> pluginsWithVersion) throws JenkinsClientException;

    void uninstallPlugin(JenkinsConfiguration configuration, String shortName) throws JenkinsClientException;

    void installOrUpdatePlugin(JenkinsConfigurationDto configuration, JenkinsClientSession session, JkPackageDto jkPackageDto) throws JenkinsClientException;
}
