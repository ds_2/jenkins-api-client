package ds2.jenkins.client.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode(of = {"value"})
@NoArgsConstructor
public class Tree<K> {
    private Tree<K> root;
    private K value;
    private List<Tree<K>> leafs;

    public Tree(Tree<K> root, K value) {
        this.root = root;
        this.value = value;
    }

    public Tree<K> moveLeafToHere(Tree<K> newLeaf){
        if (leafs == null) {
            leafs = new ArrayList<>(1);
        }
        leafs.add(newLeaf);
        newLeaf.getRoot().getLeafs().remove(newLeaf);
        newLeaf.setRoot(this);
        return newLeaf;
    }

    public Tree<K> addLeaf(K k) {
        if (leafs == null) {
            leafs = new ArrayList<>(1);
        }
        Tree<K> leaf = new Tree(this, k);
        leafs.add(leaf);
        return leaf;
    }

    public boolean isRoot() {
        return root == null;
    }

    public boolean hasLeafs() {
        return leafs != null && !leafs.isEmpty();
    }

    public String printLeaf(int indent) {
        StringBuilder stringBuilder = new StringBuilder(100);
        for (int i = 0; i < indent; ++i) {
            stringBuilder.append("  ");
        }
        if (value == null) {
            stringBuilder.append("<root>");
        } else {
            stringBuilder.append(value);
        }
        if (hasLeafs()) {
            for (Tree<K> leaf : leafs) {
                stringBuilder.append('\n').append(leaf.printLeaf(indent + 1));
            }
        }
        return stringBuilder.toString();
    }

    public Tree<K> findInTree(K k) {
        if (k == null) {
            throw new IllegalArgumentException("You cannot search for null!");
        }
        if (k.equals(value)) {
            return this;
        }
        if (leafs != null && leafs.size() > 0) {
            for (Tree<K> leaf : leafs) {
                Tree<K> foundVal = leaf.findInTree(k);
                if (foundVal != null) {
                    return foundVal;
                }
            }
        }
        return null;
    }
}
