package ds2.jenkins.client.api.dto.updatecenter;

import ds2.jenkins.client.api.dto.PluginData;
import lombok.*;

import javax.xml.bind.annotation.*;
import java.util.Map;
import java.util.Set;

@Setter
@Getter
@ToString
@XmlRootElement(name = "installablePackage")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class InstallablePackage {
    private String name;
    private String url;
    private String title;
    private String wiki;
    private Set<String> categories;
    private String version;
    private PluginData installed;
    private String minimumJavaVersion;
    private String requiredCore;
    private String excerpt;
    private String sourceId;
    private boolean compatibleWithInstalledVersion;
    private Map<String,String> dependencies;
    @XmlElementWrapper
    private Set<InstallablePackage> neededDependencies;
}
