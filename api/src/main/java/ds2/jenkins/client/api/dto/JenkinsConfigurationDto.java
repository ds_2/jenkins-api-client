package ds2.jenkins.client.api.dto;

import ds2.jenkins.client.api.JenkinsConfiguration;
import lombok.*;

import java.net.URL;

@Getter
@Setter
@ToString(exclude = {"apiToken"})
@EqualsAndHashCode
@NoArgsConstructor
public class JenkinsConfigurationDto implements JenkinsConfiguration {
    private String username;
    private char[] apiToken;
    private URL jenkinsBaseUrl;
    private int readTimeoutSeconds = 15;
    private int connectTimeoutSeconds = 10;
    private CrumbData crumbData;
    private String jenkinsSession;
}
