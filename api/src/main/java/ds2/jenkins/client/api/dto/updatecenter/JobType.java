package ds2.jenkins.client.api.dto.updatecenter;

public enum JobType {
    ConnectionCheckJob,
    InstallationJob;
}
