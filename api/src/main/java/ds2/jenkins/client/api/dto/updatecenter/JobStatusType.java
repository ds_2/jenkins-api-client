package ds2.jenkins.client.api.dto.updatecenter;

public enum JobStatusType {
    SuccessButRequiresRestart,
    Failure,
    Pending,
    Installing,
    Success;
}
