package ds2.jenkins.client.api.dto.updatecenter;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@ToString
@XmlRootElement(name = "updateCenterJobStatus")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class JobStatus {
    private JobStatusType type;
    private boolean success;
}
