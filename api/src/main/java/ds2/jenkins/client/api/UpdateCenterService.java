package ds2.jenkins.client.api;

import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.UpdateRecommendation;
import ds2.jenkins.client.api.dto.updatecenter.Job;

import java.util.List;

public interface UpdateCenterService {
    List<String> getAllUpdateCenters(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;

    List<UpdateRecommendation> getUpdates(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;

    List<Job> getJobs(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;
}
