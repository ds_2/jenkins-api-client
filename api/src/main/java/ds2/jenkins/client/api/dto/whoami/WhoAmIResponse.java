package ds2.jenkins.client.api.dto.whoami;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Setter
@Getter
@ToString
@XmlRootElement(name = "whoAmI")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class WhoAmIResponse {
    private boolean anonymous;
    private String authority;
    private String name;
    private String toString;
}
