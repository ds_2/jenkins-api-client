package ds2.jenkins.client.api.dto;

import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(of = {"shortName"})
@XmlRootElement(name = "plugin")
@Builder
@ToString(of = {"shortName", "longName", "active", "version", "backupVersion"})
public class PluginData implements ShortNamed {
    private boolean active;
    /**
     * The old version we could fallback to.
     */
    private String backupVersion;
    private boolean bundled;
    private boolean deleted;
    private Set<PluginDependency> dependencies;
    private boolean downgradable;
    private boolean enabled;
    private boolean hasUpdate;
    private String longName;
    private boolean pinned;
    private String requiredCoreVersion;
    private String shortName;
    private DynamicLoadable supportsDynamicLoad;
    private String url;
    private String minimumJavaVersion;
    /**
     * Installed or requested version.
     */
    private String version;

}
