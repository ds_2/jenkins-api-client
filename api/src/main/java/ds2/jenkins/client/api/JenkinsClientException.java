package ds2.jenkins.client.api;

public class JenkinsClientException extends Exception {
    public JenkinsClientException(String message) {
        super(message);
    }

    public JenkinsClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
