package ds2.jenkins.client.api;

import ds2.jenkins.client.api.dto.ShortNamed;

import java.io.Serializable;

public interface JkPackage extends ShortNamed, Serializable {
    String getVersion();
}
