package ds2.jenkins.client.api.dto;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class JobData {
    private String _class;
    private String name;
    private String url;
    private BallColor color;
}
