package ds2.jenkins.client.api;

import ds2.jenkins.client.api.dto.CrumbData;
import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JenkinsInfo;
import ds2.jenkins.client.api.dto.whoami.WhoAmIResponse;

public interface JenkinsService {

    JenkinsInfo getInfo(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;

    CrumbData getCrumb(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;

    void scheduleSafeRestart(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;

    void setQuietMode(JenkinsConfiguration configuration, JenkinsClientSession session, boolean quiet) throws JenkinsClientException;

    WhoAmIResponse whoAmI(JenkinsConfiguration configuration, JenkinsClientSession session) throws JenkinsClientException;
}
