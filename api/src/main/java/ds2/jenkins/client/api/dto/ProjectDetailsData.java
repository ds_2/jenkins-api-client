package ds2.jenkins.client.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;
import java.net.URL;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"name"})
@XmlRootElement(name = "project")
public class ProjectDetailsData {
    private String description;
    private String displayName;
    private String fullName;
    private String name;
    private String url;
    private boolean buildable;
    private String color;
    private boolean inQueue;
    private boolean keepDependencies;
    private long nextBuildNumber;
    private boolean concurrentBuild;
}
