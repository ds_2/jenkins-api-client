package ds2.jenkins.client.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "name")
public class PluginValidation {
    private String name;
    private String version;
    private Mode mode;

    public enum Mode {
        missing, old
    }
}
