package ds2.jenkins.client.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = {"name"})
public class CookieData implements Serializable {
    private String domain;
    private String value;
    private String path;
    private String name;
    private Date expiry;
    private int maxAge;
    private String comment;
}
