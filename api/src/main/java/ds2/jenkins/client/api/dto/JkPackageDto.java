package ds2.jenkins.client.api.dto;

import ds2.jenkins.client.api.JkPackage;
import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class JkPackageDto implements JkPackage {
    private String shortName;
    private String version;

    public JkPackageDto(String shortName) {
        this.shortName = shortName;
    }
}
