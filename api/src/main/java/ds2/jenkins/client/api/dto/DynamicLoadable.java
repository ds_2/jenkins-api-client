package ds2.jenkins.client.api.dto;

public enum DynamicLoadable {
    MAYBE, YES, NO;
}
