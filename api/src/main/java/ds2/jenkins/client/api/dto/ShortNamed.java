package ds2.jenkins.client.api.dto;

public interface ShortNamed {
    String getShortName();
}
