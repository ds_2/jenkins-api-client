package ds2.jenkins.client.api;

import ds2.jenkins.client.api.dto.JenkinsClientSession;
import ds2.jenkins.client.api.dto.JobResult;
import ds2.jenkins.client.api.dto.JobState;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface JobsService {
    /**
     * Downloads the xml file data for this job configuration.
     *
     * @param configuration the jenkins configuration
     * @param jobName       the job name
     * @return the xml data for this job
     * @throws JenkinsClientException if an error occurred
     */
    String downloadJobConfiguration(JenkinsConfiguration configuration, JenkinsClientSession session, String jobName) throws JenkinsClientException;

    List<String> getAllJobNames(JenkinsConfiguration configuration, JenkinsClientSession session, Set<JobState> states, Set<JobResult> results) throws JenkinsClientException;

    void scheduleBuild(JenkinsConfiguration configuration, JenkinsClientSession session, String jobName, Map<String, Object> buildParams) throws JenkinsClientException;

    void enableJob(JenkinsConfiguration configuration, JenkinsClientSession session, String name, boolean enable) throws JenkinsClientException;
}
