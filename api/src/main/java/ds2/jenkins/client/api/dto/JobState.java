package ds2.jenkins.client.api.dto;

public enum JobState {
    INACTIVE, RUNNING, SCHEDULED;
}
