#!/bin/bash

CONTAINERNAME="test-jenkins-production"
IMGID="jenkins/jenkins:lts"
JKPORT=12345

function init(){
    docker pull $IMGID
    docker volume create --label keep=true --label jee=jenkins --label state=alpha --name jenkins_data
    echo "Your disk is here:"
    docker volume inspect jenkins_data
}

function run(){
    docker run --name $CONTAINERNAME -d -v jenkins_data:/var/jenkins_home -p ${JKPORT}:8080 -p 50000:50000 --env JAVA_OPTS=-Dhudson.footerURL=http://mycompany.com --env JENKINS_SLAVE_AGENT_PORT=50001 -e EXTRA_JAVA_OPTIONS=-Xmx2g --rm $IMGID
    echo "You can browse http://localhost:${JKPORT}/ soon.."
}

function log(){
    docker logs -f -t $CONTAINERNAME
}

function enterContainer(){
    docker exec -it $CONTAINERNAME /bin/bash
}

function removeOldContainer(){
    # usually not needed as we run --rm above
    docker rm $CONTAINERNAME
}

case $1 in
init)
    init
;;
crumb)
    CRUMB=$(curl localhost:${JKPORT}/crumbIssuer/api/xml?xpath=concat\(//crumbRequestField,%22:%22,//crumb\))
    echo "Crumb is: $CRUMB"
  ;;
run)
    run
;;
stop)
    docker stop $CONTAINERNAME
;;
log)
    log
;;
enter)
    enterContainer
;;
remove)
    removeOldContainer
;;
*)
    echo "$0 (init|run|stop|log|enter)"
    echo "   init = downloads the docker image"
    echo "   run = runs the docker image"
    echo "   stop = stops the docker image"
    echo "   log = opens the docker log file for this container"
    echo "   enter = enters the running container via bash"
    echo "   remove = removes the old container"
;;
esac
