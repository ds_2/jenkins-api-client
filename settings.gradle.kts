rootProject.name = "ds2-jenkins-client-project"
//enableFeaturePreview("STABLE_PUBLISHING")
include("api", "client")
findProject(":api")!!.name = "ds2-jenkins-client-api"
findProject(":client")!!.name = "ds2-jenkins-client-impl"
include("installtask")
findProject(":installtask")!!.name = "ds2-jenkins-client-installtask"
