# DS/2 Jenkins API Client

A client for accessing the jenkins remote api.

## How to test

Run the docker image:

    ./runDocker.sh init
    ./runDocker.sh run
    
This will install a local jenkins server on your machine. Use http://localhost:12345/ to access it.

Set up an admin user on initial access. For our tests, we use the user ciadm with password admin.

## Short notes

* the URL for the plugin updates is https://updates.jenkins.io/update-center.json

### get currently installed plugins

    curl -kLsS -u ciadm:admin "http://localhost:12345/pluginManager/api/json?pretty=1&tree=plugins\[shortName,longName,version\]"

If plugin has an update, the new version is included. You can now simply "install" the update:

    java -jar /root/jenkins-cli.jar -s http://127.0.0.1:8080/ install-plugin ${UPDATE_LIST};
    java -jar /root/jenkins-cli.jar -s http://127.0.0.1:8080/ safe-restart;
    
### Install Plugins

    curl -s -L http://updates.jenkins-ci.org/update-center.json | sed '1d;$d' > plugins.json

    curl -s -L http://updates.jenkins-ci.org/update-center.json | sed '1d;$d' | curl -s -X POST -H 'Accept: application/json' -d @- http://localhost:8080/updateCenter/byId/default/postBack

    curl --user ciadm:admin localhost:12345'/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)'